<?php


use Phinx\Migration\AbstractMigration;

class ChatSetting extends AbstractMigration
{

    public function up(){
        $table = $this->table('chat-settings');
        $table
            ->addColumn('chat_id', 'biginteger')
            ->addColumn('nsfw', 'boolean')
            ->addColumn('disabled', 'text')
            ->addTimestamps()
            ->create();
    }

    public function down(){
        $this->table('chat-settings')->drop();
    }
}

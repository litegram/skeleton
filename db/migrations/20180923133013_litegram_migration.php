<?php

use Phinx\Migration\AbstractMigration;

/**
 * Databases needed for core functions of Litegram.
 */
class LitegramMigration extends AbstractMigration{
    
    public function up(){
        // API key table
        $table = $this->table('api-keys');
        $table->addColumn('name', 'string')
            ->addColumn('key', 'string')
            ->addTimestamps()
            ->create();
        
        // Cache table
        $table = $this->table('cache');
        $table->addColumn('name', 'string')
            ->addColumn('expiry', 'datetime')
            ->addColumn('value', 'text')
            ->addTimestamps()
            ->create();
    }

    public function down(){
        $this->schema->drop('api-keys');
        $this->schema->drop('cache');
    }
}

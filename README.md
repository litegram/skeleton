# Litegram

Slim-based framework for Telegram bots.

## Reuqirements

- PHP 7.1+
- A database server compatible with Eloquent and Phinx
- Composer

## Setup

1. Create the project somewhere in your server.
    ```bash
    composer create-project litegram/litegram --remove-vcs
    ```

2. Fill in the .env file with your settings.

3. Run the database migrations.
   ```bash
   ./vendor/bin/phinx init
   $EDITOR phinx.yml
   ./vendor/bin/phinx migrate
   ```

4. Register the webhook (command coming soon; for now do it manually)
    ```bash
    curl "https://api.telegram.org/bot<TOKEN>/setWebhook?url=example.com/bot/webhook/<TOKEN>"
    ```

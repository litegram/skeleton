*Usage*: {{name}}

    Provides a list of all available commands.

*Usage*: {{name}} [command name]

    Provides a help message for that command

Author: Joaquin
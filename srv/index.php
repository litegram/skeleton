<?php

use App\Kernel;

// For the PHP development server.
if(PHP_SAPI == 'cli-server'){
    $url  = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];

    if(is_file($file))
        return false;
}

require_once __DIR__ . '/../vendor/autoload.php';

$k = new Kernel(__DIR__.'/..', Kernel::ENV_PROD);

$k->runHttp();
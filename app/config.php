<?php

namespace Litegram;

use function DI\env;
use function DI\add;
use App\Http\Controllers\TestController;

return [

    // Monolog
    //

    'monolog.name'       => 'Litegram Skeleton',
    'monolog.handlers'   => \DI\add([
        //
    ]),
    'monolog.processors' => \DI\add([
        //
    ]),

    // Eloquent
    //

    'db' => [
        'driver'    => env('DB_DRIVER'),
        'host'      => env('DB_HOST'),
        'database'  => env('DB_NAME'),
        'username'  => env('DB_USER'),
        'password'  => env('DB_PASS'),
        'charset'   => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix'    => ''
    ],

    // Telegram
    //

    'telegram.modules' => add([
        //
    ]),

    'telegram.token' => env('TG_TOKEN'),

    // Http
    //

    'http.controllers' => add([
        
    ]),

    // Command line
    //

    'cli.commands' => add([
        //
    ]),

];
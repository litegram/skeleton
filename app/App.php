<?php

namespace App;

use Litegram\App as StockApp;
use DI\ContainerBuilder;

class App extends StockApp{

    /**
     * @{inheritdoc}
     */
    protected function configureContainer(ContainerBuilder $builder){
        parent::configureContainer($builder);

        // Add app definitions.
        $builder->addDefinitions(__DIR__.'/config.php');
    }
}